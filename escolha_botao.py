#!/usr/bin/env python
# -*- coding: utf-8 -*-

import string 

def decisao(palavra, resto):
	"""retorna a decisão de qual botão desarma a bomba"""
	if(resto == 11):
		return "Encontrado! O botão "+palavra[0]+" desarma a bomba!"
	else:
		return "O botão "+palavra[0]+" oferece perigo"

def calculo(palavras, somatorio):
	""" Retorna o resto da divisão do código pela cor """
	return decisao(palavras,somatorio[1]%somatorio[0])

def tabela_traducao():
	"""Gera a tabela de tradução"""
	return {j:i+1 for i,j in enumerate(string.ascii_uppercase)}
	
def somatorio_traducao(valores):
	"""Traduz e retorna somatório"""
	itens = tabela_traducao()
	somatorio = []
	palavras = valores.split()
	
	for unidade in palavras:
		somatorio.append(sum([itens[i] for i in unidade]))
	return calculo(palavras, somatorio)
	
if __name__ == "__main__":
	print(somatorio_traducao('VERMELHO IAJNLITLUNAYDHFAA'))
	print(somatorio_traducao('AZUL EFDLUMHBNDRRTM'))
	print(somatorio_traducao('VERDE ZTRAHDSICFQH'))
	print(somatorio_traducao('AMARELO QPSKXDLPWFLAAKHY'))
